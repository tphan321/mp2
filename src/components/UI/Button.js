import classes from "./Button.module.css"
import PropTypes from "prop-types";

const Button = (props) => {
    return <button className={classes.button} onClick={props.onClick}>{props.children}</button>
}

Button.propTypes = {
    Button: PropTypes.element,
}

export default Button;