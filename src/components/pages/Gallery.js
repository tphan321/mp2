import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import Card from "../UI/Card";
import classes from './Pokemon.module.css';
import Button from "../UI/Button";
import axios from "axios";
import PropTypes from "prop-types";

const Gallery = () => {

    const [pokemonData, setPokemonData] = useState([]);
    const [pokemons, setPokemons] = useState([]);

    useEffect(() => {
        if (pokemonData.length === 0) {
            axios.get(`https://pokeapi.co/api/v2/pokemon?limit=151&offset=0`).then((response) => {
                setPokemonData(response["data"].results);
            })
        }
        if (pokemonData.length > 0 && pokemons.length === 0) {
            setPokemons([]);
            for (let i = 0; i < pokemonData.length; i++) {
                axios.get(pokemonData[i].url).then((response) => {
                    const { abilities, height, id, name, sprites, types, weight } = response.data;
                    let img = sprites["other"]["dream_world"]["front_default"];
                    setPokemons((prev) => [...prev, { id, name, abilities, height, img, types, weight }])
                });
            }
        }
    }, [pokemonData, pokemons.length])

    const [typeFilter, setTypeFilter] = useState("All");
    let types = ["All", "Grass", "Poison", "Fire", "Flying", "Water", "Bug", "Normal", "Electric", "Ground", "Fairy", "Fighting", "Rock", "Psychic", "Steel", "Ice", "Ghost", "Dragon"]

    let typeFilterHandler = (e) => {
        setTypeFilter(e.target.innerText);
    }

    let filteredPokemons = []
    if (typeFilter === "All") {
        filteredPokemons = pokemons
    } else {
        for (let i = 0; i < pokemons.length; i++) {
            for (let j = 0; j < pokemons[i].types.length; j++) {
                if (pokemons[i].types[j].type.name === typeFilter.toLowerCase()) {
                    filteredPokemons.push(pokemons[i]);
                }
            }
        }
    }

    return (
        <>
            <section>
                <h1>The Gallery</h1>
                {
                    types.map((type) => {
                        return <Button key={type} onClick={typeFilterHandler}>{type}</Button>
                    })
                }
            </section>

            <section>
                <div>
                    {
                        filteredPokemons.map((pokemon) => {
                            return (
                                <Card key={pokemon.id}>
                                    <h4 className={classes["text-color"]}>{`${pokemon.id}.${pokemon.name.toUpperCase()}`}</h4>
                                    <Link to={{
                                        pathname: `/pokemon/${pokemon.id}`,
                                        state: pokemons
                                    }}>
                                        <figure>
                                            <img alt="pokemon" src={pokemon.img} />
                                        </figure>
                                    </Link>
                                </Card>
                            )
                        })
                    }
                </div>
            </section>
        </>
    )
}

Gallery.propTypes = {
    types: PropTypes.array,
    filteredPokemons: PropTypes.arrayOf(PropTypes.object),
    Card: PropTypes.element,
    pokemonData: PropTypes.arrayOf(PropTypes.object),
    pokemons: PropTypes.arrayOf(PropTypes.object),
    typeFilter: PropTypes.bool
}

export default Gallery;
