import React from 'react';
import { Link, useParams } from 'react-router-dom';
import { useLocation } from 'react-router-dom';
import Card from '../UI/Card';
import Button from '../UI/Button';
import classes from './PokemonDetail.module.scss';
import PropTypes from "prop-types";

const Detail = () => {
    const { pokemonId } = useParams();
    const location = useLocation();
    const pokemons = location.state;
    let pokemon;
    for (let i = 0; i < pokemons.length; i++) {
        if (pokemons[i].id === (+pokemonId)) {
            pokemon = pokemons[i];
        }
    }

    let types = [];
    let abilities = [];
    for (let i = 0; i < pokemon.types.length; i++) {
        types.push(pokemon.types[i].type.name);
    }

    for (let i = 0; i < pokemon.abilities.length; i++) {
        abilities.push(pokemon.abilities[i].ability.name);
    }

    return (
        <div>
            {(pokemon.id - 1) > 0 && <Link to={{
                pathname: `/pokemon/${pokemon.id - 1}`,
                state: pokemons
            }}>
                <Button>Prev</Button>
            </Link>}

            <Card>
                <h2>{pokemon.id} &#8212; {pokemon.name.toUpperCase()}</h2>
                <img alt="pokemon" src={pokemon.img} />
                <h4 className={classes.h4}>Height: {pokemon.height / 10} m</h4>
                <h4 className={classes.h4}>Weight: {pokemon.weight / 10} kgs</h4>
                <h4 className={classes.h4}>Types:
                    {
                        types.map((typeName) => {
                            return <span className={classes[`${typeName}`]} key={typeName}> {typeName} </span>
                        })
                    }
                </h4>
                <h4 className={classes.h4}> Abilities:
                    {
                        abilities.map((ability) => {
                            return <span className={classes["ability-badge"]} key={ability}> {ability} </span>
                        })
                    }
                </h4>
            </Card>
            {
                (pokemon.id + 1) < pokemons.length && <Link to={{
                    pathname: `/pokemon/${pokemon.id + 1}`,
                    state: pokemons
                }}>
                    <Button>Next</Button>
                </Link>
            }

        </div>
    )
}

Detail.propTypes = {
    types: PropTypes.array,
    abilities: PropTypes.array,
    Card: PropTypes.element,
    pokemons: PropTypes.arrayOf(PropTypes.object),
    pokemon: PropTypes.object,
    pokemonId: PropTypes.number
}

export default Detail