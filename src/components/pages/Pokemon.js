import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import Card from "../UI/Card";
import axios from "axios";
import classes from './Pokemon.module.css';
import { useHistory, useLocation } from "react-router-dom";
import Button from "../UI/Button";
import PropTypes from "prop-types";

const sortPokemonsById = (pokemons, ascending) => {
    return pokemons.sort((pokemonA, pokemonB) => {
        if(!ascending) {
            return pokemonA.id > pokemonB.id ? 1: -1;
        } else {
            return pokemonA.id < pokemonB.id ? 1: -1;
        }
    });
}

const sortPokemonsByHeight = (pokemons, ascending) => {
    return pokemons.sort((pokemonA, pokemonB) => {
        if(!ascending) {
            return pokemonA.height > pokemonB.height ? 1: -1;
        } else {
            return pokemonA.height < pokemonB.height ? 1: -1;
        }
    });
}

const List = () => {
    const [userSearch, setUserSearch] = useState("");
    const [pokemonData, setPokemonData] = useState([]);
    const [pokemons, setPokemons] = useState([]);

    const history = useHistory();
    const location = useLocation();

    const searchHandler = (event) => {
        let search = event.target.value.trim();
        if (search.length > 0)
            setUserSearch(event.target.value);
    }


    useEffect(() => {
        if(pokemonData.length === 0) { 
            axios.get(`https://pokeapi.co/api/v2/pokemon?limit=151&offset=0`).then((response) => {
                setPokemonData(response["data"].results);
            })
        }
        if(pokemonData.length > 0 && pokemons.length === 0) {
            setPokemons([]);
            for(let i = 0; i < pokemonData.length; i++) {
                axios.get(pokemonData[i].url).then((response) => {
                    const {abilities, height, id, name, sprites, types, weight} = response.data;
                    let img = sprites["other"]["dream_world"]["front_default"];
                    setPokemons((prev) => [...prev, {id, name, abilities, height, img, types, weight}])
                });
            }
        }
    }, [userSearch, pokemonData, pokemons.length])


    const [sortBy, setSortBy] = useState("id");
    const [subPokemons, setSubPokemons] = useState([]);
    useEffect(() => {
        if(userSearch.trim().length > 0) {
            let tmp = []
            for(let i = 0; i < pokemons.length; i++) {
                if(pokemons[i].name.includes(userSearch)) {
                    tmp.push(pokemons[i]);
                }
            }
            setSubPokemons(tmp);
        }
    }, [userSearch, pokemons])

    const queryParams = new URLSearchParams(location.search);
    const sortingTerm = queryParams.get('sort') === 'asc';
    let sortedPokemons = []


    if(sortBy === "id") {
        sortedPokemons = sortPokemonsById(subPokemons, sortingTerm)
    } else {
        sortedPokemons = sortPokemonsByHeight(subPokemons, sortingTerm);
    }
    
    const dropDownHandler = (e) => {
        setSortBy(e.target.value);
    }

    const changeSortingHandler = () => {
        history.push(`/pokemon?sort=` + ( sortingTerm ? 'desc':'asc'));
    };

    return (
        <>
            <section>
                <h1>Pokemon</h1>
                <input className={classes.input} type='text' onChange={searchHandler} />
                <div>
                    <Button onClick={changeSortingHandler}>Sort {sortingTerm ? 'Ascending':'Descending'}</Button>
                </div>
                <label> sort by </label>
                <select
                    onChange={dropDownHandler}
                >
                    <option value="id">Id</option>
                    <option value="height">Height</option>
                </select>
            </section>

            <section>
                <div>
                    {
                        sortedPokemons.map((pokemon) => {
                            return (
                                <Card key={pokemon.id}>
                                    <h4 className={classes["text-color"]}>{`${pokemon.id}.${pokemon.name.toUpperCase()}`}</h4>
                                    <span>Height: {pokemon.height}</span>
                                    <Link  to={{
                                        pathname: `/pokemon/${pokemon.id}`,
                                        state: pokemons
                                    }}>
                                        <figure>
                                            <img alt="pokemon" src={pokemon.img} />
                                        </figure>
                                    </Link>
                                </Card>
                            )
                        })
                    }
                </div>
            </section>
        </>
    )
}

List.propTypes = {
    userSearch: PropTypes.string,
    sortBy: PropTypes.string,
    Card: PropTypes.element,
    pokemonData: PropTypes.arrayOf(PropTypes.object),
    pokemons: PropTypes.arrayOf(PropTypes.object),
    subPokemons: PropTypes.arrayOf(PropTypes.object),
    dropDownHandler: PropTypes.func,
    changeSortingHandler: PropTypes.func,
    sortingTerm: PropTypes.bool,
    sortPokemonsById: PropTypes.func,
    sortPokemonsByHeight: PropTypes.func
}

export default List;