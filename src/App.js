import './App.css';
import Gallery from './components/pages/Gallery';
import List from './components/pages/Pokemon';
import MainHeader from './components/MainHeader';
import PokemonDetail from './components/pages/PokemonDetail';

import { Redirect, Route, Switch } from 'react-router-dom';

function App() {
	return (
		<div className="App">
			<MainHeader />
			<main>
				<Switch>
					<Route path="/" exact>
						<Redirect to="/pokemon" />
					</Route>
					<Route path="/pokemon" exact>
						<List />
					</Route>
					<Route path="/gallery">
						<Gallery />
					</Route>
					<Route path="/pokemon/:pokemonId">
						<PokemonDetail />
					</Route>
					<Route path="*">
						<Redirect to="/pokemon" />
					</Route>
				</Switch>
			</main>
		</div>
	);
}

export default App;
